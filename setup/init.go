package setup

import (
	"go-template-training/config"
	"go-template-training/service/util/logs"
)

type setup struct {
	appConfig *config.Config
	log       logs.Log
}

func New(appConfig *config.Config, log logs.Log) *setup {
	return &setup{appConfig, log}
}
