package implement

import (
	"go-template-training/service/staff"
	wrp "go-template-training/service/staff/wrapper"
	"go-template-training/service/util"
	"go-template-training/service/util/logs"
	"go-template-training/service/validator"
)

type implementation struct {
	*StaffServiceConfig
}

type StaffServiceConfig struct {
	Validator    validator.Validator
	RepoStaff    util.Repository
	RepoRedis    util.RepositoryRedis
	UUID         util.UUID
	DateTime     util.DateTime
	FilterString util.FilterString
	Log          logs.Log
}

func New(config *StaffServiceConfig) (service staff.Service) {
	return &wrp.Wrapper{
		Service: &implementation{config},
	}
}
